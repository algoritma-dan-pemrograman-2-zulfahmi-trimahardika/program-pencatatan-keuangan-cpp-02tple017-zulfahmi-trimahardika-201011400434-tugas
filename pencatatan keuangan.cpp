#include <iostream>
#include <stdlib.h>
using namespace std;
int cetak(int);

char *bulan[12]={"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
int income[12], jml_peng[12], nom_peng[12][20], tot_peng[12], sisa[12];
string peng[12][24];

int main(){
	cout<<"Nama:\tZulfahmi Trimahardika"<<endl<<"NIM:\t201011400434"<<endl<<"Kelas:\t02TPLE017"<<endl;
	cout<<"====================================================================================="<<endl;
	cout<<"\t\t\tProgram Pencatatan Keuangan Pribadi"<<endl;
	cout<<"====================================================================================="<<endl;
	
	int tahun, x=0;
	char pilih, pilih2, nama[20];
	string tgl_lahir;
	
	cout<<"Input Nama:\t\t\t\t";
	cin.getline(nama,20);
	cout<<"Input Tanggal Lahir (dd-mm-yyyy):\t";
	cin>>tgl_lahir;
	
	cout<<endl<<"-------------------------------------------------------------------------------"<<endl<<endl;

	cout<<"Input Tahun Saat Ini: ";
	cin>>tahun;
	cout<<endl;
	
	for(int i=0; i<12; i++){
		x=i;
		cout<<"Input Income Bulan "<<bulan[i]<<":\t\t";
		cin>>income[i];
		
		cout<<"Input Jumlah Pengeluaran Bulanan:\t";
		cin>>jml_peng[i];
		tot_peng[i]=0;
		
		for (int j=0; j<jml_peng[i]; j++){
			cout<<"Input Nama Pengeluaran Bulanan:\t\t";
			cin>>peng[i][j];
			cout<<"Input Nominal Pengeluaran Bulanan:\t";
			cin>>nom_peng[i][j];
			tot_peng[i]+=nom_peng[i][j];
		}
		sisa[i]=income[i]-tot_peng[i];
		cout<<endl;
		
		if (i<11){
			do{
				cout<<"Apakah ingin menampilkan rincian yang telah diinput? [Y/T] ";
				cin>>pilih;		
			}while(pilih!='y' && pilih!='Y' && pilih!='t' && pilih!='T');
			
			if (pilih == 'y' || pilih == 'Y'){
				cetak(i);
				
				do{
					cout<<"Apakah ingin menginput lagi? [Y/T] ";
					cin>>pilih2;
				}while(pilih2!='y' && pilih2!='Y' && pilih2!='t' && pilih2!='T');
				
				
				if (pilih2 == 'y' || pilih2 == 'Y'){
					system("cls");
					continue;
				} else if (pilih2 == 't' || pilih2 == 'T'){
					break;
				}
			} else if (pilih == 't' || pilih == 'T'){
				do{
					cout<<"Apakah ingin menginput lagi? [Y/T] ";
					cin>>pilih2;
				}while(pilih2!='y' && pilih2!='Y' && pilih2!='t' && pilih2!='T');
				
				cout<<endl;
				
				if (pilih2 == 'y' || pilih2 == 'Y'){
					system("cls");
					continue;
				} else if (pilih2 == 't' || pilih2 == 'T'){
					break;
				} 
			}
		} else{
			break;
		}
	}
	
	system("cls");
	
	cout<<"======================================================"<<endl;
	cout<<"Tahun "<<tahun<<endl;
	cout<<"Nama:\t\t"<<nama<<endl;
	cout<<"Tanggal Lahir:\t"<<tgl_lahir<<endl;
	
	for(int i=0; i<=x; i++){
		cetak(i);
	}
	
}

int cetak(int i){
	cout<<"======================================================"<<endl;
	cout<<"Rincian Keuangan Bulan "<<bulan[i]<<endl;
	cout<<"======================================================"<<endl;
	
	cout<<"Income Bulan "<<bulan[i]<<":\t\t"<<income[i]<<endl;
	
	for (int j=0; j<jml_peng[i]; j++){
		cout<<"Nama Pengeluaran Bulanan:\t"<<peng[i][j]<<endl;
		cout<<"Nominal Pengeluaran Bulanan:\t"<<nom_peng[i][j]<<endl<<endl;
	}
	cout<<"Total Pengeluaran Bulanan:\t"<<tot_peng[i]<<endl;
	
	cout<<"Total Sisa Bulanan:\t\t"<<sisa[i]<<endl<<endl;	
}
